import { CopyOutlined } from '@ant-design/icons';
import { Col, FloatButton, Popover, Row, Typography, message } from "antd";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import Cookies from "universal-cookie";

// warning : CC7070

const rawItems: Item[] = [
  { id: 1, name: 'The Sad Onion', isFavorite: false },
  { id: 2, name: 'The Inner Eye', isFavorite: false },
  { id: 3, name: 'Spoon Bender', isFavorite: false },
  { id: 4, name: 'Cricket\'s Head', isFavorite: false },
  { id: 5, name: 'My Reflection', isFavorite: false },
  { id: 6, name: 'Number One', isFavorite: false },
  { id: 7, name: 'Blood of the Martyr', isFavorite: false },
  { id: 8, name: 'Brother Bobby', isFavorite: false },
  { id: 9, name: 'Skatole', isFavorite: false },
  { id: 10, name: 'Halo of Flies', isFavorite: false },
  { id: 11, name: '1up!', isFavorite: false },
  { id: 12, name: 'Magic Mushroom', isFavorite: false },
  { id: 13, name: 'The Virus', isFavorite: false },
  { id: 14, name: 'Roid Rage', isFavorite: false },
  { id: 15, name: '<3', isFavorite: false },
  { id: 16, name: 'Raw Liver', isFavorite: false },
  { id: 17, name: 'Skeleton Key', isFavorite: false },
  { id: 18, name: 'A Dollar', isFavorite: false },
  { id: 19, name: 'Boom!', isFavorite: false },
  { id: 20, name: 'Transcendence', isFavorite: false },
  { id: 21, name: 'The Compass', isFavorite: false },
  { id: 22, name: 'Lunch', isFavorite: false },
  { id: 23, name: 'Dinner', isFavorite: false },
  { id: 24, name: 'Dessert', isFavorite: false },
  { id: 25, name: 'Breakfast', isFavorite: false },
  { id: 26, name: 'Rotten Meat', isFavorite: false },
  { id: 27, name: 'Wooden Spoon', isFavorite: false },
  { id: 28, name: 'The Belt', isFavorite: false },
  { id: 29, name: 'Mom\'s Underwear', isFavorite: false },
  { id: 30, name: 'Mom\'s Heels', isFavorite: false },
  { id: 31, name: 'Mom\'s Lipstick', isFavorite: false },
  { id: 32, name: 'Wire Coat Hanger', isFavorite: false },
  { id: 33, name: 'The Bible', isFavorite: false },
  { id: 34, name: 'The Book of Belial', isFavorite: false },
  { id: 35, name: 'The Necronomicon', isFavorite: false },
  { id: 36, name: 'The Poop', isFavorite: false },
  { id: 37, name: 'Mr. Boom', isFavorite: false },
  { id: 38, name: 'Tammy\'s Head', isFavorite: false },
  { id: 39, name: 'Mom\'s Bra', isFavorite: false },
  { id: 40, name: 'Kamikaze', isFavorite: false },
  { id: 41, name: 'Mom\'s Pad', isFavorite: false },
  { id: 42, name: 'Bob\'s Rotten Head', isFavorite: false },
  { id: 43, name: 'Teleport', isFavorite: false },
  { id: 44, name: 'Yum Heart', isFavorite: false },
  { id: 45, name: 'Lucky Foot', isFavorite: false },
  { id: 46, name: 'Doctor\'s Remote', isFavorite: false },
  { id: 47, name: 'Cupid\'s Arrow', isFavorite: false },
  { id: 48, name: 'Shoop Da Whoop!', isFavorite: false },
  { id: 49, name: 'Steven', isFavorite: false },
  { id: 50, name: 'Pentagram', isFavorite: false },
  { id: 51, name: 'Dr. Fetus', isFavorite: false },
  { id: 52, name: 'Magneto', isFavorite: false },
  { id: 53, name: 'Treasure Map', isFavorite: false },
  { id: 54, name: 'Mom\'s Eye', isFavorite: false },
  { id: 55, name: 'Lemon Mishap', isFavorite: false },
  { id: 56, name: 'Distant Admiration', isFavorite: false },
  { id: 57, name: 'Book of Shadows', isFavorite: false },
  { id: 58, name: 'The Ladder', isFavorite: false },
  { id: 59, name: 'Charm of the Vampire', isFavorite: false },
  { id: 60, name: 'The Battery', isFavorite: false },
  { id: 61, name: 'Steam Sale', isFavorite: false },
  { id: 62, name: 'Anarchist Cookbook', isFavorite: false },
  { id: 63, name: 'The Hourglass', isFavorite: false },
  { id: 64, name: 'Sister Maggy', isFavorite: false },
  { id: 65, name: 'Technology', isFavorite: false },
  { id: 66, name: 'Chocolate Milk', isFavorite: false },
  { id: 67, name: 'Growth Hormones', isFavorite: false },
  { id: 68, name: 'Mini Mush', isFavorite: false },
  { id: 69, name: 'Rosary', isFavorite: false },
  { id: 70, name: 'Cube of Meat', isFavorite: false },
  { id: 71, name: 'A Quarter', isFavorite: false },
  { id: 72, name: 'PHD', isFavorite: false },
  { id: 73, name: 'X-Ray Vision', isFavorite: false },
  { id: 74, name: 'My Little Unicorn', isFavorite: false },
  { id: 75, name: 'Book of Revelations', isFavorite: false },
  { id: 76, name: 'The Mark', isFavorite: false },
  { id: 77, name: 'The Pact', isFavorite: false },
  { id: 78, name: 'Dead Cat', isFavorite: false },
  { id: 79, name: 'Lord of the Pit', isFavorite: false },
  { id: 80, name: 'The Nail', isFavorite: false },
  { id: 81, name: 'We Need To Go Deeper!', isFavorite: false },
  { id: 82, name: 'Deck of Cards', isFavorite: false },
  { id: 83, name: 'Monstro\'s Tooth', isFavorite: false },
  { id: 84, name: 'Loki\'s Horns', isFavorite: false },
  { id: 85, name: 'Little Chubby', isFavorite: false },
  { id: 86, name: 'Spider Bite', isFavorite: false },
  { id: 87, name: 'The Small Rock', isFavorite: false },
  { id: 88, name: 'Spelunker Hat', isFavorite: false },
  { id: 89, name: 'Super Bandage', isFavorite: false },
  { id: 90, name: 'The Gamekid', isFavorite: false },
  { id: 91, name: 'Sack of Pennies', isFavorite: false },
  { id: 92, name: 'Robo-Baby', isFavorite: false },
  { id: 93, name: 'Little C.H.A.D. (Little CHAD)', isFavorite: false },
  { id: 94, name: 'The Book of Sin', isFavorite: false },
  { id: 95, name: 'The Relic', isFavorite: false },
  { id: 96, name: 'Little Gish', isFavorite: false },
  { id: 97, name: 'Little Steven', isFavorite: false },
  { id: 98, name: 'The Halo', isFavorite: false },
  { id: 99, name: 'Mom\'s Bottle of Pills', isFavorite: false },
  { id: 100, name: 'The Common Cold', isFavorite: false },
  { id: 101, name: 'The Parasite', isFavorite: false },
  { id: 102, name: 'The D6', isFavorite: false },
  { id: 103, name: 'Mr. Mega', isFavorite: false },
  { id: 104, name: 'The Pinking Shears', isFavorite: false },
  { id: 105, name: 'The Wafer', isFavorite: false },
  { id: 106, name: 'Money = Power', isFavorite: false },
  { id: 107, name: 'Mom\'s Contact', isFavorite: false },
  { id: 108, name: 'The Bean', isFavorite: false },
  { id: 109, name: 'Guardian Angel', isFavorite: false },
  { id: 110, name: 'Demon Baby', isFavorite: false },
  { id: 111, name: 'Mom\'s Knife', isFavorite: false },
  { id: 112, name: 'Ouija Board', isFavorite: false },
  { id: 113, name: '9-Volt', isFavorite: false },
  { id: 114, name: 'Dead Bird', isFavorite: false },
  { id: 115, name: 'Brimstone', isFavorite: false },
  { id: 116, name: 'Blood Bag', isFavorite: false },
  { id: 117, name: 'Odd Mushroom (thin)', isFavorite: false },
  { id: 118, name: 'Odd Mushroom (flat)', isFavorite: false },
  { id: 119, name: 'Whore of Babylon', isFavorite: false },
  { id: 120, name: 'Monster Manual/Monster Manuel', isFavorite: false },
  { id: 121, name: 'Dead Sea Scrolls', isFavorite: false },
  { id: 122, name: 'Bobby-Bomb', isFavorite: false },
  { id: 123, name: 'Razor Blade', isFavorite: false },
  { id: 124, name: 'Forget Me Now', isFavorite: false },
  { id: 125, name: 'Forever Alone', isFavorite: false },
  { id: 126, name: 'Bucket of Lard', isFavorite: false },
  { id: 127, name: 'A Pony', isFavorite: false },
  { id: 128, name: 'Bomb Bag', isFavorite: false },
  { id: 129, name: 'A Lump of Coal', isFavorite: false },
  { id: 130, name: 'Guppy\'s Paw', isFavorite: false },
  { id: 131, name: 'Guppy\'s Tail', isFavorite: false },
  { id: 132, name: 'IV Bag', isFavorite: false },
  { id: 133, name: 'Best Friend', isFavorite: false },
  { id: 134, name: 'Remote Detonator', isFavorite: false },
  { id: 135, name: 'Stigmata', isFavorite: false },
  { id: 136, name: 'Mom\'s Purse', isFavorite: false },
  { id: 137, name: 'Bob\'s Curse', isFavorite: false },
  { id: 138, name: 'Pageant Boy', isFavorite: false },
  { id: 139, name: 'Scapular', isFavorite: false },
  { id: 140, name: 'Speed Ball', isFavorite: false },
  { id: 141, name: 'Bum Friend', isFavorite: false },
  { id: 142, name: 'Guppy\'s Head', isFavorite: false },
  { id: 143, name: 'Prayer Card', isFavorite: false },
  { id: 144, name: 'Notched Axe', isFavorite: false },
  { id: 145, name: 'Infestation', isFavorite: false },
  { id: 146, name: 'Ipecac', isFavorite: false },
  { id: 147, name: 'Tough Love', isFavorite: false },
  { id: 148, name: 'The Mulligan', isFavorite: false },
  { id: 149, name: 'Technology 2', isFavorite: false },
  { id: 150, name: 'Mutant Spider', isFavorite: false },
  { id: 151, name: 'Chemical Peel', isFavorite: false },
  { id: 152, name: 'The Peeper', isFavorite: false },
  { id: 153, name: 'Habit', isFavorite: false },
  { id: 154, name: 'Bloody Lust', isFavorite: false },
  { id: 155, name: 'Crystal Ball', isFavorite: false },
  { id: 156, name: 'Spirit of the Night', isFavorite: false },
  { id: 157, name: 'Crack the Sky', isFavorite: false },
  { id: 158, name: 'Ankh', isFavorite: false },
  { id: 159, name: 'Celtic Cross', isFavorite: false },
  { id: 160, name: 'Ghost Baby', isFavorite: false },
  { id: 161, name: 'The Candle', isFavorite: false },
  { id: 162, name: 'Cat-O-Nine-Tails', isFavorite: false },
  { id: 163, name: 'D20', isFavorite: false },
  { id: 164, name: 'Harlequin Baby', isFavorite: false },
  { id: 165, name: 'Epic Fetus', isFavorite: false },
  { id: 166, name: 'Polyphemus', isFavorite: false },
  { id: 167, name: 'Daddy Longlegs', isFavorite: false },
  { id: 168, name: 'Spider Butt', isFavorite: false },
  { id: 169, name: 'Sacrificial Dagger', isFavorite: false },
  { id: 170, name: 'Mitre', isFavorite: false },
  { id: 171, name: 'Rainbow Baby', isFavorite: false },
  { id: 172, name: 'Dad\'s Key', isFavorite: false },
  { id: 173, name: 'Stem Cells', isFavorite: false },
  { id: 174, name: 'Portable Slot', isFavorite: false },
  { id: 175, name: 'Holy Water', isFavorite: false },
  { id: 176, name: 'Fate', isFavorite: false },
  { id: 177, name: 'The Black Bean', isFavorite: false },
  { id: 178, name: 'White Pony', isFavorite: false },
  { id: 179, name: 'Sacred Heart', isFavorite: false },
  { id: 180, name: 'Toothpicks', isFavorite: false },
  { id: 181, name: 'Holy Grail', isFavorite: false },
  { id: 182, name: 'Dead Dove', isFavorite: false },
  { id: 183, name: 'Blood Rights', isFavorite: false },
  { id: 184, name: 'Guppy\'s Hair Ball', isFavorite: false },
  { id: 185, name: 'Abel', isFavorite: false },
  { id: 186, name: 'SMB Super Fan', isFavorite: false },
  { id: 187, name: 'Pyro', isFavorite: false },
  { id: 188, name: '3 Dollar Bill', isFavorite: false },
  { id: 189, name: 'Telepathy For Dummies', isFavorite: false },
  { id: 190, name: 'MEAT!', isFavorite: false },
  { id: 191, name: 'Magic 8 Ball', isFavorite: false },
  { id: 192, name: 'Mom\'s Coin Purse', isFavorite: false },
  { id: 193, name: 'Squeezy', isFavorite: false },
  { id: 194, name: 'Jesus Juice', isFavorite: false },
  { id: 195, name: 'Box', isFavorite: false },
  { id: 196, name: 'Mom\'s Key', isFavorite: false },
  { id: 197, name: 'Mom\'s Eyeshadow', isFavorite: false },
  { id: 198, name: 'Iron Bar', isFavorite: false },
  { id: 199, name: 'Midas\' Touch', isFavorite: false },
  { id: 200, name: 'Humbling Bundle', isFavorite: false },
  { id: 201, name: 'Fanny Pack', isFavorite: false },
  { id: 202, name: 'Sharp Plug', isFavorite: false },
  { id: 203, name: 'Guillotine', isFavorite: false },
  { id: 204, name: 'Ball of Bandages', isFavorite: false },
  { id: 205, name: 'Champion\'s Belt', isFavorite: false },
  { id: 206, name: 'Butt Bombs', isFavorite: false },
  { id: 207, name: 'Gnawed Leaf', isFavorite: false },
  { id: 208, name: 'Spiderbaby', isFavorite: false },
  { id: 209, name: 'Guppy\'s Collar', isFavorite: false },
  { id: 210, name: 'Lost Contact', isFavorite: false },
  { id: 211, name: 'Anemic', isFavorite: false },
  { id: 212, name: 'Goat Head', isFavorite: false },
  { id: 213, name: 'Ceremonial Robes', isFavorite: false },
  { id: 214, name: 'Mom\'s Wig', isFavorite: false },
  { id: 215, name: 'Placenta', isFavorite: false },
  { id: 216, name: 'Old Bandage', isFavorite: false },
  { id: 217, name: 'Sad Bombs', isFavorite: false },
  { id: 218, name: 'Rubber Cement', isFavorite: false },
  { id: 219, name: 'Anti-Gravity', isFavorite: false },
  { id: 220, name: 'Pyromaniac', isFavorite: false },
  { id: 221, name: 'Cricket\'s Body', isFavorite: false },
  { id: 222, name: 'Gimpy', isFavorite: false },
  { id: 223, name: 'Black Lotus', isFavorite: false },
  { id: 224, name: 'Piggy Bank', isFavorite: false },
  { id: 225, name: 'Mom\'s Perfume', isFavorite: false },
  { id: 226, name: 'Monstro\'s Lung', isFavorite: false },
  { id: 227, name: 'Abaddon', isFavorite: false },
  { id: 228, name: 'Ball of Tar', isFavorite: false },
  { id: 229, name: 'Stop Watch', isFavorite: false },
  { id: 230, name: 'Tiny Planet', isFavorite: false },
  { id: 231, name: 'Infestation 2', isFavorite: false },
  { id: 232, name: 'E Coli', isFavorite: false },
  { id: 233, name: 'Death\'s Touch', isFavorite: false },
  { id: 234, name: 'Key Piece 1', isFavorite: false },
  { id: 235, name: 'Key Piece 2', isFavorite: false },
  { id: 236, name: 'Experimental Treatment', isFavorite: false },
  { id: 237, name: 'Contract From Below', isFavorite: false },
  { id: 238, name: 'Infamy', isFavorite: false },
  { id: 239, name: 'Trinity Shield', isFavorite: false },
  { id: 240, name: 'Tech.5', isFavorite: false },
  { id: 241, name: '20/20', isFavorite: false },
  { id: 242, name: 'Blue Map', isFavorite: false },
  { id: 243, name: 'BFFS!', isFavorite: false },
  { id: 244, name: 'Hive Mind', isFavorite: false },
  { id: 245, name: 'There\'s Options', isFavorite: false },
  { id: 246, name: 'Bogo Bombs', isFavorite: false },
  { id: 247, name: 'Starter Deck', isFavorite: false },
  { id: 248, name: 'Little Baggy', isFavorite: false },
  { id: 249, name: 'Magic Scab', isFavorite: false },
  { id: 250, name: 'Blood Clot', isFavorite: false },
  { id: 251, name: 'Screw', isFavorite: false },
  { id: 252, name: 'Hot Bombs', isFavorite: false },
  { id: 253, name: 'Fire Mind', isFavorite: false },
  { id: 254, name: 'Missing No.', isFavorite: false },
  { id: 255, name: 'Dark Matter', isFavorite: false },
  { id: 256, name: 'Black Candle', isFavorite: false },
  { id: 257, name: 'Proptosis', isFavorite: false },
  { id: 258, name: 'Missing Page 2', isFavorite: false },
  { id: 259, name: 'Clear Rune', isFavorite: false },
  { id: 260, name: 'Smart Fly', isFavorite: false },
  { id: 261, name: 'Dry Baby', isFavorite: false },
  { id: 262, name: 'Juicy Sack', isFavorite: false },
  { id: 263, name: 'Robo-Baby 2.0', isFavorite: false },
  { id: 264, name: 'Rotten Baby', isFavorite: false },
  { id: 265, name: 'Headless Baby', isFavorite: false },
  { id: 266, name: 'Leech', isFavorite: false },
  { id: 267, name: 'Mystery Sack', isFavorite: false },
  { id: 268, name: 'BBF', isFavorite: false },
  { id: 269, name: 'Bob\'s Brain', isFavorite: false },
  { id: 270, name: 'Best Bud', isFavorite: false },
  { id: 271, name: 'Lil Brimstone', isFavorite: false },
  { id: 272, name: 'Isaac\'s Heart', isFavorite: false },
  { id: 273, name: 'Lil Haunt', isFavorite: false },
  { id: 274, name: 'Dark Bum', isFavorite: false },
  { id: 275, name: 'Big Fan', isFavorite: false },
  { id: 276, name: 'Sissy Longlegs', isFavorite: false },
  { id: 277, name: 'Punching Bag', isFavorite: false },
  { id: 278, name: 'How To Jump', isFavorite: false },
  { id: 279, name: 'D100', isFavorite: false },
  { id: 280, name: 'D4', isFavorite: false },
  { id: 281, name: 'D10', isFavorite: false },
  { id: 282, name: 'Blank Card', isFavorite: false },
  { id: 283, name: 'Book of Secrets', isFavorite: false },
  { id: 284, name: 'Box of Spiders', isFavorite: false },
  { id: 285, name: 'Red Candle', isFavorite: false },
  { id: 286, name: 'The Jar', isFavorite: false },
  { id: 287, name: 'Flush!', isFavorite: false },
  { id: 288, name: 'Satanic Bible', isFavorite: false },
  { id: 289, name: 'Head of Krampus', isFavorite: false },
  { id: 290, name: 'Butter Bean', isFavorite: false },
  { id: 291, name: 'Magic Fingers', isFavorite: false },
  { id: 292, name: 'Converter', isFavorite: false },
  { id: 293, name: 'Pandora\'s Box', isFavorite: false },
  { id: 294, name: 'Unicorn Stump', isFavorite: false },
  { id: 295, name: 'Taurus', isFavorite: false },
  { id: 296, name: 'Aries', isFavorite: false },
  { id: 297, name: 'Cancer', isFavorite: false },
  { id: 298, name: 'Leo', isFavorite: false },
  { id: 299, name: 'Virgo', isFavorite: false },
  { id: 300, name: 'Libra', isFavorite: false },
  { id: 301, name: 'Scorpio', isFavorite: false },
  { id: 302, name: 'Sagittarius', isFavorite: false },
  { id: 303, name: 'Capricorn', isFavorite: false },
  { id: 304, name: 'Aquarius', isFavorite: false },
  { id: 305, name: 'Pisces', isFavorite: false },
  { id: 306, name: 'Eve\'s Mascara', isFavorite: false },
  { id: 307, name: 'Judas\' Shadow', isFavorite: false },
  { id: 308, name: 'Maggy\'s Bow', isFavorite: false },
  { id: 309, name: 'Holy Mantle', isFavorite: false },
  { id: 310, name: 'Thunder Thighs', isFavorite: false },
  { id: 311, name: 'Strange Attractor', isFavorite: false },
  { id: 312, name: 'Cursed Eye', isFavorite: false },
  { id: 313, name: 'Mysterious Liquid', isFavorite: false },
  { id: 314, name: 'Gemini', isFavorite: false },
  { id: 315, name: 'Cain\'s Other Eye', isFavorite: false },
  { id: 316, name: '???\'s Only Friend', isFavorite: false },
  { id: 317, name: 'Samson\'s Chains', isFavorite: false },
  { id: 318, name: 'Mongo Baby', isFavorite: false },
  { id: 319, name: 'Isaac\'s Tears', isFavorite: false },
  { id: 320, name: 'Undefined', isFavorite: false },
  { id: 321, name: 'Scissors', isFavorite: false },
  { id: 322, name: 'Breath of Life', isFavorite: false },
  { id: 323, name: 'The Polaroid', isFavorite: false },
  { id: 324, name: 'The Negative', isFavorite: false },
  { id: 325, name: 'The Ludovico Technique', isFavorite: false },
  { id: 326, name: 'Soy Milk', isFavorite: false },
  { id: 327, name: 'Godhead', isFavorite: false },
  { id: 328, name: 'Lazarus\' Rags', isFavorite: false },
  { id: 329, name: 'The Mind', isFavorite: false },
  { id: 330, name: 'The Body', isFavorite: false },
  { id: 331, name: 'The Soul', isFavorite: false },
  { id: 332, name: 'Dead Onion', isFavorite: false },
  { id: 333, name: 'Broken Watch', isFavorite: false },
  { id: 334, name: 'The Boomerang', isFavorite: false },
  { id: 335, name: 'Safety Pin', isFavorite: false },
  { id: 336, name: 'Caffeine Pill', isFavorite: false },
  { id: 337, name: 'Torn Photo', isFavorite: false },
  { id: 338, name: 'Blue Cap', isFavorite: false },
  { id: 339, name: 'Latch Key', isFavorite: false },
  { id: 340, name: 'Match Book', isFavorite: false },
  { id: 341, name: 'Synthoil', isFavorite: false },
  { id: 342, name: 'A Snack', isFavorite: false },
  { id: 343, name: 'Diplopia', isFavorite: false },
  { id: 344, name: 'Placebo', isFavorite: false },
  { id: 345, name: 'Wooden Nickel', isFavorite: false },
  { id: 346, name: 'Toxic Shock', isFavorite: false },
  { id: 347, name: 'Mega Bean', isFavorite: false },
  { id: 348, name: 'Glass Cannon', isFavorite: false },
  { id: 349, name: 'Bomber Boy', isFavorite: false },
  { id: 350, name: 'Crack Jacks', isFavorite: false },
  { id: 351, name: 'Mom\'s Pearls', isFavorite: false },
  { id: 352, name: 'Car Battery', isFavorite: false },
  { id: 353, name: 'Box of Friends', isFavorite: false },
  { id: 354, name: 'The Wiz', isFavorite: false },
  { id: 355, name: '8 Inch Nails', isFavorite: false },
  { id: 356, name: 'Incubus', isFavorite: false },
  { id: 357, name: 'Fate\'s Reward', isFavorite: false },
  { id: 358, name: 'Lil Chest', isFavorite: false },
  { id: 359, name: 'Sworn Protector', isFavorite: false },
  { id: 360, name: 'Friend Zone', isFavorite: false },
  { id: 361, name: 'Lost Fly', isFavorite: false },
  { id: 362, name: 'Scatter Bombs', isFavorite: false },
  { id: 363, name: 'Sticky Bombs', isFavorite: false },
  { id: 364, name: 'Epiphora', isFavorite: false },
  { id: 365, name: 'Continuum', isFavorite: false },
  { id: 366, name: 'Mr. Dolly', isFavorite: false },
  { id: 367, name: 'Curse of the Tower', isFavorite: false },
  { id: 368, name: 'Charged Baby', isFavorite: false },
  { id: 369, name: 'Dead Eye', isFavorite: false },
  { id: 370, name: 'Holy Light', isFavorite: false },
  { id: 371, name: 'Host Hat', isFavorite: false },
  { id: 372, name: 'Restock', isFavorite: false },
  { id: 373, name: 'Bursting Sack', isFavorite: false },
  { id: 374, name: 'No. 2', isFavorite: false },
  { id: 375, name: 'Pupula Duplex', isFavorite: false },
  { id: 376, name: 'Pay to Play', isFavorite: false },
  { id: 377, name: 'Eden\'s Blessing', isFavorite: false },
  { id: 378, name: 'Friendly Ball', isFavorite: false },
  { id: 379, name: 'Tear Detonator', isFavorite: false },
  { id: 380, name: 'Lil Gurdy', isFavorite: false },
  { id: 381, name: 'Bumbo', isFavorite: false },
  { id: 382, name: 'D12', isFavorite: false },
  { id: 383, name: 'Censer', isFavorite: false },
  { id: 384, name: 'Key Bum', isFavorite: false },
  { id: 385, name: 'Rune Bag', isFavorite: false },
  { id: 386, name: 'Seraphim', isFavorite: false },
  { id: 387, name: 'Betrayal', isFavorite: false },
  { id: 388, name: 'Zodiac', isFavorite: false },
  { id: 389, name: 'Serpent\'s Kiss', isFavorite: false },
  { id: 390, name: 'Marked', isFavorite: false },
  { id: 391, name: 'Tech X', isFavorite: false },
  { id: 392, name: 'Ventricle Razor', isFavorite: false },
  { id: 393, name: 'Tractor Beam', isFavorite: false },
  { id: 394, name: 'God\'s Flesh', isFavorite: false },
  { id: 395, name: 'Maw of the Void', isFavorite: false },
  { id: 396, name: 'Spear of Destiny', isFavorite: false },
  { id: 397, name: 'Explosivo', isFavorite: false },
  { id: 398, name: 'Chaos', isFavorite: false },
  { id: 399, name: 'Spider Mod', isFavorite: false },
  { id: 400, name: 'Farting Baby', isFavorite: false },
  { id: 401, name: 'GB Bug', isFavorite: false },
  { id: 402, name: 'D8', isFavorite: false },
  { id: 403, name: 'Purity', isFavorite: false },
  { id: 404, name: 'Athame', isFavorite: false },
  { id: 405, name: 'Empty Vessel', isFavorite: false },
  { id: 406, name: 'Evil Eye', isFavorite: false },
  { id: 407, name: 'Lusty Blood', isFavorite: false },
  { id: 408, name: 'Cambion Conception', isFavorite: false },
  { id: 409, name: 'Immaculate Conception', isFavorite: false },
  { id: 410, name: 'More Options', isFavorite: false },
  { id: 411, name: 'Crown of Light', isFavorite: false },
  { id: 412, name: 'Deep Pockets', isFavorite: false },
  { id: 413, name: 'Succubus', isFavorite: false },
  { id: 414, name: 'Fruit Cake', isFavorite: false },
  { id: 415, name: 'Teleport 2.0', isFavorite: false },
  { id: 416, name: 'Black Powder', isFavorite: false },
  { id: 417, name: 'Kidney Bean', isFavorite: false },
  { id: 418, name: 'Glowing Hour Glass', isFavorite: false },
  { id: 419, name: 'Circle of Protection', isFavorite: false },
  { id: 420, name: 'Sack Head', isFavorite: false },
  { id: 421, name: 'Night Light', isFavorite: false },
  { id: 422, name: 'Obsessed Fan', isFavorite: false },
  { id: 423, name: 'Mine Crafter', isFavorite: false },
  { id: 424, name: 'PJs', isFavorite: false },
  { id: 425, name: 'Head of the Keeper', isFavorite: false },
  { id: 426, name: 'Papa Fly', isFavorite: false },
  { id: 427, name: 'Multidimensional Baby', isFavorite: false },
  { id: 428, name: 'Glitter Bombs', isFavorite: false },
  { id: 429, name: 'My Shadow', isFavorite: false },
  { id: 430, name: 'Jar of Flies', isFavorite: false },
  { id: 431, name: 'Lil Loki', isFavorite: false },
  { id: 432, name: 'Milk!', isFavorite: false },
  { id: 433, name: 'D7', isFavorite: false },
  { id: 434, name: 'Binky', isFavorite: false },
  { id: 435, name: 'Mom\'s Box', isFavorite: false },
  { id: 436, name: 'Kidney Stone', isFavorite: false },
  { id: 437, name: 'Mega Blast', isFavorite: false },
  { id: 438, name: 'Dark Prince\'s Crown', isFavorite: false },
  { id: 439, name: 'Apple!', isFavorite: false },
  { id: 440, name: 'Lead Pencil', isFavorite: false },
  { id: 441, name: 'Dog Tooth', isFavorite: false },
  { id: 442, name: 'Dead Tooth', isFavorite: false },
  { id: 443, name: 'Linger Bean', isFavorite: false },
  { id: 444, name: 'Shard of Glass', isFavorite: false },
  { id: 445, name: 'Metal Plate', isFavorite: false },
  { id: 446, name: 'Eye of Greed', isFavorite: false },
  { id: 447, name: 'Tarot Cloth', isFavorite: false },
  { id: 448, name: 'Varicose Veins', isFavorite: false },
  { id: 449, name: 'Compound Fracture', isFavorite: false },
  { id: 450, name: 'Polydactyly', isFavorite: false },
  { id: 451, name: 'Dad\'s Lost Coin', isFavorite: false },
  { id: 452, name: 'Midnight Snack', isFavorite: false },
  { id: 453, name: 'Cone Head', isFavorite: false },
  { id: 454, name: 'Belly Button', isFavorite: false },
  { id: 455, name: 'Sinus Infection', isFavorite: false },
  { id: 456, name: 'Glaucoma', isFavorite: false },
  { id: 457, name: 'Parasitoid', isFavorite: false },
  { id: 458, name: 'Eye of Belial', isFavorite: false },
  { id: 459, name: 'Sulfuric Acid', isFavorite: false },
  { id: 460, name: 'Glyph of Balance', isFavorite: false },
  { id: 461, name: 'Analog Stick', isFavorite: false },
  { id: 462, name: 'Contagion', isFavorite: false },
  { id: 463, name: 'Finger!', isFavorite: false },
  { id: 464, name: 'Shade', isFavorite: false },
  { id: 465, name: 'Depression', isFavorite: false },
  { id: 466, name: 'Hushy', isFavorite: false },
  { id: 467, name: 'Lil Monstro', isFavorite: false },
  { id: 468, name: 'King Baby', isFavorite: false },
  { id: 469, name: 'Big Chubby', isFavorite: false },
  { id: 470, name: 'Broken Glass Cannon', isFavorite: false },
  { id: 471, name: 'Plan C', isFavorite: false },
  { id: 472, name: 'D1', isFavorite: false },
  { id: 473, name: 'Void', isFavorite: false },
  { id: 474, name: 'Pause', isFavorite: false },
  { id: 475, name: 'Smelter', isFavorite: false },
  { id: 476, name: 'Compost', isFavorite: false },
  { id: 477, name: 'Dataminer', isFavorite: false },
  { id: 478, name: 'Clicker', isFavorite: false },
  { id: 479, name: 'Mama Mega!', isFavorite: false },
  { id: 480, name: 'Wait What?', isFavorite: false },
  { id: 481, name: 'Crooked Penny', isFavorite: false },
  { id: 482, name: 'Dull Razor', isFavorite: false },
  { id: 483, name: 'Potato Peeler', isFavorite: false },
  { id: 484, name: 'Metronome', isFavorite: false },
  { id: 485, name: 'D Infinity', isFavorite: false },
  { id: 486, name: 'Eden\'s Soul', isFavorite: false },
  { id: 487, name: 'Acid Baby', isFavorite: false },
  { id: 488, name: 'YO LISTEN!', isFavorite: false },
  { id: 489, name: 'Adrenaline', isFavorite: false },
  { id: 490, name: 'Jacob\'s Ladder', isFavorite: false },
  { id: 491, name: 'Ghost Pepper', isFavorite: false },
  { id: 492, name: 'Euthanasia', isFavorite: false },
  { id: 493, name: 'Camo Undies', isFavorite: false },
  { id: 494, name: 'Duality', isFavorite: false },
  { id: 495, name: 'Eucharist', isFavorite: false },
  { id: 496, name: 'Sack of Sacks', isFavorite: false },
  { id: 497, name: 'Greed\'s Gullet', isFavorite: false },
  { id: 498, name: 'Large Zit', isFavorite: false },
  { id: 499, name: 'Little Horn', isFavorite: false },
  { id: 500, name: 'Brown Nugget', isFavorite: false },
  { id: 501, name: 'Poke Go', isFavorite: false },
  { id: 502, name: 'Backstabber', isFavorite: false },
  { id: 503, name: 'Sharp Straw', isFavorite: false },
  { id: 504, name: 'Mom\'s Razor', isFavorite: false },
  { id: 505, name: 'Bloodshot Eye', isFavorite: false },
  { id: 506, name: 'Delirious', isFavorite: false },
  { id: 507, name: 'Angry Fly', isFavorite: false },
  { id: 508, name: 'Black Hole', isFavorite: false },
  { id: 509, name: 'Bozo', isFavorite: false },
  { id: 510, name: 'Broken Modem', isFavorite: false },
  { id: 511, name: 'Mystery Gift', isFavorite: false },
  { id: 512, name: 'Sprinkler', isFavorite: false },
  { id: 513, name: 'Fast Bombs', isFavorite: false },
  { id: 514, name: 'Buddy In A Box', isFavorite: false },
  { id: 515, name: 'Lil Delirium', isFavorite: false },
  { id: 516, name: 'Jumper Cables', isFavorite: false },
  { id: 517, name: '(good item for me to) Coupon', isFavorite: false },
  { id: 518, name: 'Telekinesis', isFavorite: false },
  { id: 519, name: 'Moving Box', isFavorite: false },
  { id: 520, name: 'Technology Zero', isFavorite: false },
  { id: 521, name: 'Leprosy', isFavorite: false },
  { id: 522, name: '7 Seals', isFavorite: false },
  { id: 523, name: 'Mr. ME!', isFavorite: false },
  { id: 524, name: 'Angelic Prism', isFavorite: false },
  { id: 525, name: 'Pop!', isFavorite: false },
  { id: 526, name: 'Death\'s List', isFavorite: false },
  { id: 527, name: 'Haemolacria', isFavorite: false },
  { id: 528, name: 'Lachryphagy', isFavorite: false },
  { id: 529, name: 'Trisagion', isFavorite: false },
  { id: 530, name: 'Schoolbag', isFavorite: false },
  { id: 531, name: 'Blanket', isFavorite: false },
  { id: 532, name: 'Sacrificial Altar', isFavorite: false },
  { id: 533, name: 'Lil Spewer', isFavorite: false },
  { id: 534, name: 'Marbles', isFavorite: false },
  { id: 535, name: 'Mystery Egg', isFavorite: false },
  { id: 536, name: 'Flat Stone', isFavorite: false },
  { id: 537, name: 'Marrow', isFavorite: false },
  { id: 538, name: 'Slipped Rib', isFavorite: false },
  { id: 539, name: 'Hallowed Ground', isFavorite: false },
  { id: 540, name: 'Pointy Rib', isFavorite: false },
  { id: 541, name: 'Book of the Dead', isFavorite: false },
  { id: 542, name: 'Dad\'s Ring', isFavorite: false },
  { id: 543, name: 'Divorce Papers', isFavorite: false },
  { id: 544, name: 'Jaw Bone', isFavorite: false },
  { id: 545, name: 'Brittle Bones', isFavorite: false },
  { id: 546, name: 'Broken Shovel (Basement I)', isFavorite: false },
  { id: 547, name: 'Broken Shovel (Boss Rush)', isFavorite: false },
  { id: 548, name: 'Mom\'s Shovel', isFavorite: false },
  { id: 549, name: 'Mucormycosis', isFavorite: false },
  { id: 550, name: '2Spooky', isFavorite: false },
  { id: 551, name: 'Golden Razor', isFavorite: false },
  { id: 552, name: 'Sulfur', isFavorite: false },
  { id: 553, name: 'Fortune Cookie', isFavorite: false },
  { id: 554, name: 'Eye Sore', isFavorite: false },
  { id: 555, name: '120 Volt', isFavorite: false },
  { id: 556, name: 'It Hurts', isFavorite: false },
  { id: 557, name: 'Almond Milk', isFavorite: false },
  { id: 558, name: 'Rock Bottom', isFavorite: false },
  { id: 559, name: 'Nancy Bombs', isFavorite: false },
  { id: 560, name: 'A Bar of Soap', isFavorite: false },
  { id: 561, name: 'Blood Puppy', isFavorite: false },
  { id: 562, name: 'Dream Catcher', isFavorite: false },
  { id: 563, name: 'Paschal Candle', isFavorite: false },
  { id: 564, name: 'Divine Intervention', isFavorite: false },
  { id: 565, name: 'Blood Oath', isFavorite: false },
  { id: 566, name: 'Playdough Cookie', isFavorite: false },
  { id: 567, name: 'Orphan Socks', isFavorite: false },
  { id: 568, name: 'Eye of the Occult', isFavorite: false },
  { id: 569, name: 'Immaculate Heart', isFavorite: false },
  { id: 570, name: 'Monstrance', isFavorite: false },
  { id: 571, name: 'The Intruder', isFavorite: false },
  { id: 572, name: 'Dirty Mind', isFavorite: false },
  { id: 573, name: 'Damocles', isFavorite: false },
  { id: 574, name: 'Free Lemonade', isFavorite: false },
  { id: 575, name: 'Spirit Sword', isFavorite: false },
  { id: 576, name: 'Red Key', isFavorite: false },
  { id: 577, name: 'Psy Fly', isFavorite: false },
  { id: 578, name: 'Wavy Cap', isFavorite: false },
  { id: 579, name: 'Rocket in a Jar', isFavorite: false },
  { id: 580, name: 'Book of Virtues', isFavorite: false },
  { id: 581, name: 'Alabaster Box', isFavorite: false },
  { id: 582, name: 'The Stairway', isFavorite: false },
  { id: 583, name: 'Sol', isFavorite: false },
  { id: 584, name: 'Luna', isFavorite: false },
  { id: 585, name: 'Mercurius', isFavorite: false },
  { id: 586, name: 'Venus', isFavorite: false },
  { id: 587, name: 'Terra', isFavorite: false },
  { id: 588, name: 'Mars', isFavorite: false },
  { id: 589, name: 'Jupiter', isFavorite: false },
  { id: 590, name: 'Saturnus', isFavorite: false },
  { id: 591, name: 'Uranus', isFavorite: false },
  { id: 592, name: 'Neptunus', isFavorite: false },
  { id: 593, name: 'Pluto', isFavorite: false },
  { id: 594, name: 'Voodoo Head', isFavorite: false },
  { id: 595, name: 'Eye Drops', isFavorite: false },
  { id: 596, name: 'Act of Contrition', isFavorite: false },
  { id: 597, name: 'Member Card', isFavorite: false },
  { id: 598, name: 'Battery Pack', isFavorite: false },
  { id: 599, name: 'Mom\'s Bracelet', isFavorite: false },
  { id: 600, name: 'The Scooper', isFavorite: false },
  { id: 601, name: 'Ocular Rift', isFavorite: false },
  { id: 602, name: 'Boiled Baby', isFavorite: false },
  { id: 603, name: 'Freezer Baby', isFavorite: false },
  { id: 604, name: 'Eternal D6', isFavorite: false },
  { id: 605, name: 'Bird Cage', isFavorite: false },
  { id: 606, name: 'Larynx', isFavorite: false },
  { id: 607, name: 'Lost Soul', isFavorite: false },
  { id: 608, name: 'Blood Bombs', isFavorite: false },
  { id: 609, name: 'Lil Dumpy', isFavorite: false },
  { id: 610, name: 'Bird\'s Eye', isFavorite: false },
  { id: 611, name: 'Lodestone', isFavorite: false },
  { id: 612, name: 'Rotten Tomato', isFavorite: false },
  { id: 613, name: 'Birthright', isFavorite: false },
  { id: 614, name: 'Red Stew', isFavorite: false },
  { id: 615, name: 'Genesis', isFavorite: false },
  { id: 616, name: 'Sharp Key', isFavorite: false },
  { id: 617, name: 'Booster Pack', isFavorite: false },
  { id: 618, name: 'Mega Mush', isFavorite: false },
  { id: 619, name: 'Knife Piece 1', isFavorite: false },
  { id: 620, name: 'Knife Piece 2', isFavorite: false },
  { id: 621, name: 'Death Certificate', isFavorite: false },
  { id: 622, name: 'Bot Fly', isFavorite: false },
  { id: 623, name: 'Meat Cleaver', isFavorite: false },
  { id: 624, name: 'Evil Charm', isFavorite: false },
  { id: 625, name: 'Dogma', isFavorite: false },
  { id: 626, name: 'Purgatory', isFavorite: false },
  { id: 627, name: 'Stitches', isFavorite: false },
  { id: 628, name: 'R Key', isFavorite: false },
  { id: 629, name: 'Knockout Drops', isFavorite: false },
  { id: 630, name: 'Eraser', isFavorite: false },
  { id: 631, name: 'Yuck Heart', isFavorite: false },
  { id: 632, name: 'Urn of Souls', isFavorite: false },
  { id: 633, name: 'Akeldama', isFavorite: false },
  { id: 634, name: 'Magic Skin', isFavorite: false },
  { id: 635, name: 'Revelation', isFavorite: false },
  { id: 636, name: 'Consolation Prize', isFavorite: false },
  { id: 637, name: 'Tinytoma', isFavorite: false },
  { id: 638, name: 'Brimstone Bombs', isFavorite: false },
  { id: 639, name: '4.5 Volt', isFavorite: false },
  { id: 640, name: 'Fruity Plum', isFavorite: false },
  { id: 641, name: 'Plum Flute', isFavorite: false },
  { id: 642, name: 'Star of Bethlehem', isFavorite: false },
  { id: 643, name: 'Cube Baby', isFavorite: false },
  { id: 644, name: 'Vade Retro', isFavorite: false },
  { id: 645, name: 'False PHD', isFavorite: false },
  { id: 646, name: 'Spin to Win', isFavorite: false },
  { id: 647, name: 'Vasculitis', isFavorite: false },
  { id: 648, name: 'Giant Cell', isFavorite: false },
  { id: 649, name: 'Tropicamide', isFavorite: false },
  { id: 650, name: 'Card Reading', isFavorite: false },
  { id: 651, name: 'Quints', isFavorite: false },
  { id: 652, name: 'Tooth and Nail', isFavorite: false },
  { id: 653, name: 'Binge Eater', isFavorite: false },
  { id: 654, name: 'Guppy\'s Eye', isFavorite: false },
  { id: 655, name: 'Strawman', isFavorite: false },
  { id: 656, name: 'Dad\'s Note', isFavorite: false },
  { id: 657, name: 'Sausage', isFavorite: false },
  { id: 658, name: 'Options?', isFavorite: false },
  { id: 659, name: 'Candy Heart', isFavorite: false },
  { id: 660, name: 'A Pound of Flesh', isFavorite: false },
  { id: 661, name: 'Redemption', isFavorite: false },
  { id: 662, name: 'Spirit Shackles', isFavorite: false },
  { id: 663, name: 'Cracked Orb', isFavorite: false },
  { id: 664, name: 'Empty Heart', isFavorite: false },
  { id: 665, name: 'Astral Projection', isFavorite: false },
  { id: 666, name: 'C Section', isFavorite: false },
  { id: 667, name: 'Lil Abaddon', isFavorite: false },
  { id: 668, name: 'Montezuma\'s Revenge', isFavorite: false },
  { id: 669, name: 'Lil Portal', isFavorite: false },
  { id: 670, name: 'Worm Friend', isFavorite: false },
  { id: 671, name: 'Bone Spurs', isFavorite: false },
  { id: 672, name: 'Hungry Soul', isFavorite: false },
  { id: 673, name: 'Jar of Wisps', isFavorite: false },
  { id: 674, name: 'Soul Locket', isFavorite: false },
  { id: 675, name: 'Friend Finder', isFavorite: false },
  { id: 676, name: 'Inner Child', isFavorite: false },
  { id: 677, name: 'Glitched Crown', isFavorite: false },
  { id: 678, name: 'Belly Jelly', isFavorite: false },
  { id: 679, name: 'Sacred Orb', isFavorite: false },
  { id: 680, name: 'Sanguine Bond', isFavorite: false },
  { id: 681, name: 'The Swarm', isFavorite: false },
  { id: 682, name: 'Heartbreak', isFavorite: false },
  { id: 683, name: 'Bloody Gust', isFavorite: false },
  { id: 684, name: 'Salvation', isFavorite: false },
  { id: 685, name: 'Vanishing Twin', isFavorite: false },
  { id: 686, name: 'Twisted Pair', isFavorite: false },
  { id: 687, name: 'Azazel\'s Rage', isFavorite: false },
  { id: 688, name: 'Echo Chamber', isFavorite: false },
  { id: 689, name: 'Isaac\'s Tomb', isFavorite: false },
  { id: 690, name: 'Vengeful Spirit', isFavorite: false },
  { id: 691, name: 'Esau Jr.', isFavorite: false },
  { id: 692, name: 'Berserk!', isFavorite: false },
  { id: 693, name: 'Dark Arts', isFavorite: false },
  { id: 694, name: 'Abyss', isFavorite: false },
  { id: 695, name: 'Supper', isFavorite: false },
  { id: 696, name: 'Stapler', isFavorite: false },
  { id: 697, name: 'Suplex!', isFavorite: false },
  { id: 698, name: 'Bag of Crafting', isFavorite: false },
  { id: 699, name: 'Flip', isFavorite: false },
  { id: 700, name: 'Lemegeton', isFavorite: false },
  { id: 701, name: 'Sumptorium', isFavorite: false },
  { id: 702, name: 'Keeper\'s Sack (Keeper\'s Elbow)', isFavorite: false },
  { id: 703, name: 'Keeper\'s Kin', isFavorite: false },
  { id: 704, name: 'Keeper\'s Box', isFavorite: false },
  { id: 705, name: 'Everything Jar', isFavorite: false },
  { id: 706, name: 'TMTRAINER', isFavorite: false },
  { id: 707, name: 'Anima Sola', isFavorite: false },
  { id: 708, name: 'Spindown Dice', isFavorite: false },
  { id: 709, name: 'Hypercoagulation', isFavorite: false },
  { id: 710, name: 'IBS', isFavorite: false },
  { id: 711, name: 'Hemoptysis', isFavorite: false },
  { id: 712, name: 'Ghost Bombs', isFavorite: false },
  { id: 713, name: 'Gello', isFavorite: false },
  { id: 714, name: 'Decap Attack', isFavorite: false },
  { id: 715, name: 'Stye', isFavorite: false },
  { id: 716, name: 'Glass Eye', isFavorite: false },
  { id: 717, name: 'Mom\'s Ring', isFavorite: false },
].reverse();

const warnings: number[] = [656];

class CookieManager {
  private cookies: Cookies;
  private cookieName: string;

  constructor(cookieName: string) {
    this.cookies = new Cookies();
    this.cookieName = cookieName;
  }

  set(value: any, options?: object): void {
    this.cookies.set(this.cookieName, value, options);
  }

  get(): any {
    if (this.cookies.get(this.cookieName) === undefined) {
      this.cookies.set(this.cookieName, []);
    }
    return this.cookies.get(this.cookieName);
  }
}

interface Item {
  id: number
  name: string
  isFavorite: boolean
  isRoom?: boolean
}

const ItemDiv: React.FC<{
  item: Item,
  selectedId: number,
  setSelectedId: (id: number) => void,
  room: Number[],
  setRoom: Dispatch<SetStateAction<Number[]>>,
  selectedSearch: Item | null,
}> = ({ item, selectedId, setSelectedId, selectedSearch, room, setRoom }) => {
  const favCookies = new CookieManager("favItems");
  const batteryCookies = new CookieManager("battery");
  const battery = batteryCookies.get() as boolean;
  const [hover, setHover] = useState(false);
  const [hoverFavorite, setHoverFavorite] = useState(false);
  const [hoverRoom, setHoverRoom] = useState(false);
  const [forceUpdate, setForceUpdate] = useState(0);
  const height = 25;

  const deltaSelected = selectedId - item.id;
  const isSelected = (deltaSelected === 0 && selectedSearch === null) || (selectedSearch !== null && selectedSearch.id === item.id);

  return (
    <div id={`item-${item.id}`} style={{ display: "flex", flexDirection: "row", alignItems: "center", marginBottom: 6, position: 'relative' }}>
      <div
        style={{
          backgroundColor: warnings.includes(item.id) ? "#CC7070" : hoverRoom ? "#9ABCDC" : hoverFavorite ? '#AB8D3F' : hover || isSelected ? '#B0F2B6' : item.isFavorite ? '#AB8D3F' : item.isRoom ? "#9ABCDC" : "#909090",
          height: height,
          marginRight: 10,
          borderRadius: 10,
          border: hoverRoom ? "1px solid #9ABCDC" : hoverFavorite ? '1px solid #AB8D3F' : hover ? '1px solid #B0F2B6' : '1px solid transparent',
          boxShadow: hoverRoom ? "0 0 10px #9ABCDC" : hoverFavorite ? '0 0 10px gold' : hover ? '0 0 10px green' : 'none',
          transition: 'border 0.1s, box-shadow 0.1s',
          cursor: "pointer",
          position: 'relative',
          display: 'flex',
          flexDirection: 'row',
          marginLeft: 10,
        }}
      >
        <div
          onClick={() => setSelectedId(item.id)}
          onMouseEnter={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          style={{
            width: height + 10,
            height: "100%",
            backgroundColor: 'rgba(0,0,0,0.4)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: 15,
            borderRadius: 10,
            paddingLeft: 5,
            paddingRight: 5,
            color: deltaSelected > 0 ? '#B0F2B6' : deltaSelected < 0 ? '#FDB0AA' : '#FF6961',
          }}
        >
          {selectedId === -1 ? "" : deltaSelected === 0 ? '-' : battery ? deltaSelected % 2 ? "⦸" : (Math.abs(deltaSelected) / 2).toFixed(0) : Math.abs(deltaSelected)}
        </div>

        <div
          onClick={() => setSelectedId(item.id)}
          onMouseEnter={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          style={{
            color: 'black',
            fontSize: 15,
            textAlign: 'center',
            lineHeight: `${height}px`,
            paddingRight: 2,
            paddingLeft: 2,
          }}
        >
          {item.id === 656 && (
            <Popover
              content={
                <Typography.Text type="danger">
                  This item is not rollable
                </Typography.Text>
              }
            >
              {item.name}
            </Popover>
          )}

          {item.id !== 656 && (
            <>
              {item.name}
            </>
          )}
        </div>

        <div
          onMouseEnter={() => setHoverFavorite(true)}
          onMouseLeave={() => setHoverFavorite(false)}
          onClick={() => {
            item.isFavorite = !item.isFavorite;
            const favItems = favCookies.get() as number[];
            if (item.isFavorite) {
              favItems.push(item.id);
            } else {
              favItems.splice(favItems.indexOf(item.id), 1);
            }
            favCookies.set(favItems);
            setForceUpdate(forceUpdate + 1);
          }}
          style={{
            width: height,
            height: height,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            color: 'white',
            fontSize: 15,
            borderRadius: 10,
          }}
        >
          <text
            style={{
              cursor: "pointer",
              color: item.isFavorite ? "gold" : "white",
            }}
          >
            {item.isFavorite ? "★" : "☆"}
          </text>
        </div>

        <div
          onMouseEnter={() => setHoverRoom(true)}
          onMouseLeave={() => setHoverRoom(false)}
          onClick={() => {
            item.isRoom = !item.isRoom;
            const newRoom = room
            if (item.isRoom) {
              newRoom.push(item.id)
            } else {
              newRoom.splice(newRoom.indexOf(item.id), 1);
            }
            console.log(newRoom)
            setRoom(newRoom)
            setForceUpdate(forceUpdate + 1);
          }}
          style={{
            width: height,
            height: height,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            color: 'white',
            fontSize: 15,
          }}
        >
          <text
            style={{
              cursor: "pointer",
              color: item.isRoom ? "blue" : "white",
            }}
          >
            {item.isRoom ? "⍓" : "⍌"}
          </text>
        </div>
      </div>

      <div style={{ color: "white", cursor: "default" }}>
        ▶
      </div>
    </div>
  )
};


function App() {
  const favCookies = new CookieManager("favItems");
  const favItems = favCookies.get() as number[];
  const batteryCookies = new CookieManager("battery");
  const battery = batteryCookies.get() as boolean;
  const [selected, setSelected] = useState<Item | null>(null)
  const [room, setRoom] = useState<Number[]>([])
  const [selectedSearch, setSelectedSearch] = useState<Item | null>(null)
  const [searchText, setSearchText] = useState<string>("")
  const [favOnly, setFavOnly] = useState<boolean>(false)
  const [forceUpdate, setForceUpdate] = useState(0)
  const items = rawItems

  const onClickItemDiv = (id: number) => {
    const item = items.find((item) => item.id === id)
    if (item) {
      if (selected?.id === item.id) {
        setSelected(null)
      } else {
        setSelected(item)
      }
    }
  }

  const filteredItems = items
    .map((item) => ({ ...item, isFavorite: favItems.includes(item.id), isRoom: room?.includes(item.id) }))
    .filter((item) => item.name.toLowerCase().includes(searchText.toLowerCase()))
    .filter((item) => searchText !== "" || !favOnly || item.isFavorite || item.isRoom || item.id === (selected?.id ?? -1))

  useEffect(() => {
    setSelectedSearch(searchText !== "" ? filteredItems[0] ?? null : null)
    scrollToSelected()
  }, [searchText])

  useEffect(() => {
    setSearchText("")
    const input = document.querySelector("input")
    if (input) {
      input.value = ""
    }

    scrollToSelected()
  }, [selected])

  const scrollToSelected = () => {
    if (selected) {
      const itemDiv = document.getElementById(`item-${selected.id}`);
      if (itemDiv) {
        itemDiv.scrollIntoView({ behavior: "smooth", block: "center" });
      }
    }
  }

  return (
    <div style={{ backgroundColor: "#101010", width: "100%", minHeight: "100vh" }}>
      <FloatButton
        shape="square"
        style={{ width: 100, height: 100 }}
        icon={<CopyOutlined />}
        description="Copy for Oneiroi"
        onClick={() => {
          const favItems = favCookies.get() as number[];

          var text = selected?.name ?? ""
          if (text === "") {
            return
          }

          if (battery) {
            text += " [Car Battery active]"
          }

          text += " | "
          favItems.forEach((id) => {
            if (id >= (selected?.id ?? -1)) {
              return
            }

            var rolls = (selected?.id ?? -1) - id
            if (battery && rolls % 2 === 1) {
              return
            }

            if (battery) {
              rolls /= 2
            }

            const item = items.find((item) => item.id === id)
            if (item) {
              text += `${rolls} rolls -> ${item.name} | `
            }
          })

          navigator.clipboard.writeText(text).then(() => {
            message.success({
              content: `Copied to clipboard: \n${text}`,
              style: {
                marginTop: "10vh",
              },
            });
          });
        }}
      />
      <Row>
        <Col span={5} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
          {searchText !== "" && (
            <Typography.Text style={{ color: "white", fontSize: 20 }}>
              Press Enter to select first item
            </Typography.Text>
          )}
        </Col>

        <Col span={7}>
          <Row
            style={{
              backgroundColor: "#404040",
              width: "100%",
              height: "100%",
              borderRadius: "0px 0px 15px 15px",
              padding: 10,
            }}
          >
            <Col span={22}>
              <input
                type="text"
                placeholder="Search"
                onChange={(e) => setSearchText(e.target.value)}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    if (selectedSearch) {
                      setSelected(selectedSearch)
                      setSearchText("")
                    }
                    const input = document.querySelector("input")
                    if (input) {
                      input.value = ""
                    }
                  }
                }}
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundColor: "transparent",
                  color: "white",
                  border: "none",
                  fontSize: 20,
                  outline: "none",
                  textAlign: "center",
                }} />
            </Col>

            <Col span={2} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <Typography.Text
                style={{ color: "white", fontSize: 20, cursor: "pointer" }}
                onClick={() => {
                  setSearchText("")
                  const input = document.querySelector("input")
                  if (input) {
                    input.value = ""
                  }
                }}
              >
                ✖
              </Typography.Text>
            </Col>
          </Row>
        </Col>

        <Col span={3} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
          <Typography.Text
            style={{ color: favOnly ? "gold" : "white", fontSize: 20, cursor: "pointer", paddingLeft: 20 }}
            onClick={() => setFavOnly(!favOnly)}
          >
            {favOnly ? "★" : "☆"} Favorites only
          </Typography.Text>
        </Col>

        <Col span={3} style={{ display: "flex", justifyContent: "flex-start", alignItems: "center" }}>
          <Typography.Text
            style={{ paddingLeft: 20, color: "white", fontSize: 20, cursor: "pointer" }}
            onClick={() => {
              favCookies.set([])
              setForceUpdate(forceUpdate + 1)
            }}
          >
            ⦸ Reset favorites
          </Typography.Text>
        </Col>

        <Col span={3} style={{ display: "flex", justifyContent: "flex-start", alignItems: "center" }}>
          <Typography.Text
            style={{ paddingLeft: 20, color: "white", fontSize: 20, cursor: "pointer" }}
            onClick={() => {
              setRoom([])
              setForceUpdate(forceUpdate + 1)
            }}
          >
            ⦸ Reset room items
          </Typography.Text>
        </Col>

        <Col span={3} style={{ display: "flex", justifyContent: "flex-start", alignItems: "center" }}>
          <Typography.Text
            style={{ paddingLeft: 20, fontSize: 20, cursor: "pointer", color: battery ? "gold" : "white" }}
            onClick={() => {
              batteryCookies.set(!battery)
              setForceUpdate(forceUpdate + 1)
            }}
          >
            {battery ? "Car Battery active" : "Car Battery inactive"}
          </Typography.Text>
        </Col>
      </Row>

      <div style={{ display: "flex", flexWrap: "wrap", alignItems: "flex-start", margin: 10 }}>
        {filteredItems.map((item) => (
          <ItemDiv key={item.id} item={item} selectedId={selected?.id ?? -1} setSelectedId={onClickItemDiv} room={room} setRoom={setRoom} selectedSearch={selectedSearch} />
        ))}
      </div>
    </div>
  )
}

export default App
